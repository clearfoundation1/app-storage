<?php

/**
 * Bind Mount Mappings summary view.
 *
 * @category   apps
 * @package    storage
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('storage');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(
    lang('storage_source_directory'),
    lang('storage_target_directory'),
    lang('storage_file_system_type')
);

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$anchors = array(anchor_custom('/app/storage/bind_mount/bind_mount_directories/', lang('storage_bind_mount')));

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

foreach ($bind_mapping_details as $details) {
    
    foreach ($details as $source => $store) {

        $source_encoded = strtr(base64_encode($source),  '+/=', '-_.');
        $source_dir_partition = '['.$store['partition'].']'.$store['source_dir'];
        $item['title'] = $source;
        $item['action'] = '';
        $item['anchors'] = button_set(
            array(anchor_custom('/app/storage/bind_mount/unmount/' . $source_encoded, lang('unmount')))
        );
        $item['details'] = array(
            $source_dir_partition,
            $store['target_dir'],
            $store['file_type']
        );

        $items[] = $item;
    }
}

sort($items);

///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

echo summary_table(
    lang('storage_bind_mappings'),
    $anchors,
    $headers,
    $items
);
