<?php

/**
 * Bind Mount
 *
 * @category   apps
 * @package    Storage
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('storage');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FORM
///////////////////////////////////////////////////////////////////////////////

echo form_open('storage/bind_mount/bind_mount_directories/');
echo form_header(lang('storage_bind_mount'));
echo field_input('source_mount_point', '', lang('storage_source_folder'), FALSE);
echo field_input('target_folder', '', lang('storage_target_folder'), FALSE);

echo field_button_set(array(form_submit_custom('submit', lang('storage_bind_mount')), anchor_cancel('/app/storage')));

echo form_footer();
echo form_close();