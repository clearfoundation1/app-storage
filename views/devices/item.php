<?php

/**
 * Partition summary view.
 *
 * @category   apps
 * @package    storage
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('storage');

///////////////////////////////////////////////////////////////////////////////
// General information
///////////////////////////////////////////////////////////////////////////////

$device_encoded = strtr(base64_encode($device),  '+/=', '-_.');

echo form_open('storage/devices/view');
echo form_header(lang('base_settings'));

echo field_view(lang('storage_device'), $device, lang('storage_size'));
echo field_input('size', $details['size'] . ' ' . $details['size_units'], lang('storage_size'), TRUE);
echo field_input('remaining_space', $remaining_space.' GB', 'Remaining Space', TRUE);
echo field_input('identifier', $details['identifier'], lang('storage_model'), TRUE);

if ($label_type['device_label']) {
    echo field_input('device_label_type', $label_type['device_label'], lang('storage_device_label'), TRUE);

}

echo form_footer();
echo form_close();


///////////////////////////////////////////////////////////////////////////////
// Show create partition if disk is partitionless and not mounted
///////////////////////////////////////////////////////////////////////////////

/*if (!$details['in_use']) {
    echo form_open('storage/devices/create_data_drive/' . $device_encoded);
    echo form_header(lang('base_create'));

    echo field_view(lang('storage_mount_point'), $storage_base);
    echo field_dropdown('type', $types, $type, lang('storage_file_system'));

    echo field_button_set(
        array(
            form_submit_custom('submit', lang('base_create')),
            anchor_cancel('/app/storage/devices')
        )
    );

    echo form_footer();
    echo form_close();
 
}*/

///////////////////////////////////////////////////////////////////////////////
//  Create partition if disk is partitionless and not mounted by parted command
///////////////////////////////////////////////////////////////////////////////

/*if (!$details['in_use']) {
    echo form_open('storage/devices/create_device_partition/' . $device_encoded);
    echo form_header(lang('create_partition'));

    echo field_dropdown('partition_type', $partition_types, $partition_type, lang('partition_type'));
    echo field_dropdown('file_type', $types, $type, lang('storage_file_system'));
    echo field_input('partition_size', 100, lang('partition_size'), FALSE);


    echo field_button_set(
        array(
            form_submit_custom('submit', lang('base_create')),
            anchor_cancel('/app/storage/devices')
        )
    );

    echo form_footer();
    echo form_close();
    return;
}
*/
?>
<style type="text/css">
    .btn-group .btn{ font-size: 0.8em; padding: 2px 10px; }
</style>
<?php
///////////////////////////////////////////////////////////////////////////////
// Partitions
///////////////////////////////////////////////////////////////////////////////


$headers = array(
    '',
    lang('storage_size'),
    lang('storage_file_system'),
    lang('storage_bootable'),
    lang('storage_mount')
);

// Initialize button
$device_encoded_with_gpt = $device.'='.'gpt';
$device_encoded_gpt = strtr(base64_encode($device_encoded_with_gpt),  '+/=', '-_.');
$device_encoded_with_msdos = $device.'='.'msdos';
$device_encoded_msdos = strtr(base64_encode($device_encoded_with_msdos),  '+/=', '-_.');

// format of in use
if ($details['in_use']) {
    $options = array('disabled' => 'disabled');
    $initialize_anchor = anchor_custom('/app/storage/devices/format_device/' . $device_encoded, lang('device_format'), 'high', $options);
} else {
    $initialize_anchor = anchor_multi(
        array (
            '/app/storage/devices/format_device/' . $device_encoded_gpt.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_gpt'),
            '/app/storage/devices/format_device/' . $device_encoded_msdos.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_msdos')
        ),
        lang('device_format')
    );

}


$anchors = array($initialize_anchor, anchor_custom('/app/storage/devices/create_device_partition/'. $device_encoded, lang('storage_new_partition')), anchor_custom('/app/storage/devices', lang('base_return_to_summary')));


foreach ($details['partitioning']['partitions'] as $id => $partition_info) {
    $device_id_encoded = $device.'='.$id;
    $device_id_encoded = strtr(base64_encode($device_id_encoded),  '+/=', '-_.');

    // partition Id
    $partition_id = $device.''.$id;
    $partition_id_encode = strtr(base64_encode($partition_id),  '+/=', '-_.');

    // Encoded with File system type 
    $partition_id_ext3 = $device.''.$id.'=ext3';
    $partition_id_encode_ext3 = strtr(base64_encode($partition_id_ext3),  '+/=', '-_.');
    $partition_id_ext4 = $device.''.$id.'=ext4';
    $partition_id_encode_ext4 = strtr(base64_encode($partition_id_ext4),  '+/=', '-_.');
    $partition_id_xfs = $device.''.$id.'=xfs';
    $partition_id_encode_xfs = strtr(base64_encode($partition_id_xfs),  '+/=', '-_.');


    // TODO: discuss icon strategy
    $bootable_icon = ($partition_info['is_bootable']) ? '<span class="fa fa-check">&nbsp;</span>' : '';

    if (empty($partition_info['mount_point']))
        $mount = ($partition_info['is_lvm']) ? lang('storage_lvm') : '';
    else
        $mount = $partition_info['mount_point'];

    if ($partition_info['is_mounted'] || $partition_info['flags'] == 'boot' || $partition_info['is_pvs'] != NULL || $partition_info['is_swap'] != NULL) {
        $option = array('disabled' => 'disabled');

        if ($partition_info['flags'] == 'boot' || $partition_info['is_pvs'] != NULL || $partition_info['is_swap'] != NULL || $mount == '/') {
            $mount_anchor = anchor_custom('/app/storage/devices/mount_points/' . $partition_id_encode, lang('storage_mount_umount'), 'high', $option);
        } else {
            $mount_anchor = anchor_custom('/app/storage/devices/mount_points/' . $partition_id_encode, lang('storage_mount_umount'));

        }
    } else {
        
        if ($partition_info['is_swap']) {
            $option = array('disabled' => 'disabled');
            $mount_anchor = anchor_custom('/app/storage/devices/mount_points/' . $partition_id_encode, lang('storage_mount_umount'), 'high', $option);
        } else {
            $mount_anchor = anchor_custom('/app/storage/devices/mount_points/' . $partition_id_encode, lang('storage_mount_umount'));
        }
    }

    $option = array('disabled' => 'disabled');
    if ($partition_info['flags'] == 'boot' || $partition_info['is_pvs'] != NULL || $partition_info['is_mounted'] != NULL || $partition_info['is_swap'] != NULL) {

        $button = array(anchor_custom('/app/storage/devices/delete/' . $device_id_encoded, lang('delete_partition'), 'high', $option), $mount_anchor, anchor_custom('/app/storage/devices/format_partition/' . $partition_id_encode, lang('partiton_format'), 'high', $option));
    } else {
        $format_partition_acchors = anchor_multi(
            array (
                '/app/storage/devices/format_partition/' . $partition_id_encode_ext3.'' => lang('storage_partition_format_by') . ': ' . lang('storage_format_by_ext3'),
                '/app/storage/devices/format_partition/' . $partition_id_encode_ext4.'' => lang('storage_partition_format_by') . ': ' . lang('storage_format_by_ext4'),
                '/app/storage/devices/format_partition/' . $partition_id_encode_xfs.'' => lang('storage_partition_format_by') . ': ' . lang('storage_format_by_xfs')
            ),
            lang('partiton_format')
        );
        $button = array(anchor_custom('/app/storage/devices/delete/' . $device_id_encoded, lang('delete_partition')), $mount_anchor, $format_partition_acchors);
    }



    $item['title'] = $device;
    $item['action'] = '';
    $item['anchors'] = button_set($button);
    $item['details'] = array(
        $id,
        round($partition_info['size']) . ' ' . $partition_info['size_units'],
        $partition_info['file_system'],
        $bootable_icon,
        $mount
    );

    $items[] = $item;
}

sort($items);

$options['no_action'] = FALSE;
$options = array(
    'id' => 'storage_partitions_summary',
    'responsive' => array(1 => 'none')
);

echo summary_table(
    lang('storage_partitions'),
    $anchors,
    $headers,
    $items,
    $options
);

