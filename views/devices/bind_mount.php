<?php

/**
 * Bind Mount
 *
 * @category   apps
 * @package    Storage
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('storage');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FORM
///////////////////////////////////////////////////////////////////////////////

$device_encoded = strtr(base64_encode($device_name),  '+/=', '-_.');
$partition_encoded = strtr(base64_encode($partition),  '+/=', '-_.');

echo form_open('storage/devices/bind_mount/'. $partition_encoded);
echo form_header(lang('storage_bind_mount'));
echo field_input('partition_id', $partition, lang('mount_partition'), TRUE);
echo field_dropdown('source_mount_point', $mount_points, '', lang('storage_source_folder'));
echo field_input('target_folder', '', lang('storage_target_folder'), FALSE);

echo field_button_set(array(form_submit_custom('submit', lang('storage_bind_mount')), anchor_cancel('/app/storage/devices/mount_points/'.$partition_encoded)));

echo form_footer();
echo form_close();