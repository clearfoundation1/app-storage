<?php
/**
 * Logical volume summary view.
 *
 * @category   apps
 * @package    storage
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('storage');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array('',lang('storage_lvm_group_size'),lang('storage_lvm_avalilable_size'),lang('storage_lvm_extendreduce'), '');

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////
if(empty($logical_volumes)){
   $anchors = array(anchor_custom('/app/storage/logical_volume/create_lvm/', lang('storage_lvm_active')));
}

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////


$i = 1;
foreach ($logical_volumes as $key => $value) {

        $device_encoded_lvm = strtr(base64_encode($value['lv_path']),  '+/=', '-_.');
        $formex = form_open('/storage/logical_volume/extend_lvm/'. $device_encoded_lvm);
        $sub = field_button_set(array(form_submit_custom('submit', lang('storage_lvm_update'))));

        $size = str_replace("GiB", " ", $value['lv_size']);
        $size = str_replace("<", " ", $size);
        $lv_total_size = "<div style='display:none'>".field_input('lv_total_size', trim($size), "", TRUE)."</div>";
        $lvextend = "$formex".''.$lv_total_size.''.field_input('lv_size', trim($size), "", FALSE)."$sub".form_close();

        $form = form_open('/storage/logical_volume/delete_lvm/'. $device_encoded_lvm);
        $button = field_button_set(array(form_submit_custom('submit',lang('storage_lvm_delete'))));
        $vg_name = "<div style='display:none'>".field_input('vg_name', trim($value['vg_name']), "", TRUE)."</div>";
        $delete_action = "$form".''.$vg_name.''."$button".form_close();

        $item1['title'] = $device;
        $item1['action'] = '';
        $item1['anchors'] = button_set(array());
        $item1['details'] = array($i,$value['vg_size'],$value['lv_size'], $lvextend, $delete_action);

        $itemss[] = $item1;
        $i++;      
}

sort($itemss);

$options = array(
    'id' => 'slogical_volumes',
  /*  'responsive' => array(1 => 'none')*/
);
$options['no_action'] = TRUE;

///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

echo summary_table(
    lang('storage_lvm_title'),
    $anchors,
    $headers,
    $itemss,
    $options
);
